#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define MAX_PLAYERS 1000
#define MAX_LINE_LENGTH 1000

typedef struct {
    char name[100];
    char club[100];
    int age;
    int potential;
    char photoURL[200];
    // Add additional fields as needed
} Player;

int main() {
    // Mengunduh dataset menggunakan command 'kaggle'
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");

    // Menunggu beberapa detik agar proses unduhan selesai
    sleep(5);

    // Mengekstrak file zip
    system("unzip fifa-player-stats-database.zip");

    // Menghapus file zip yang tidak diperlukan
    system("rm fifa-player-stats-database.zip");

    FILE *file;
    char line[MAX_LINE_LENGTH];
    Player players[MAX_PLAYERS];
    int numPlayers = 0;

    file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL) {
        printf("Failed to open file.\n");
        return 1;
    }

    // Skip header line
    fgets(line, MAX_LINE_LENGTH, file);

    while (fgets(line, MAX_LINE_LENGTH, file) != NULL) {
        char *token;
        token = strtok(line, ",");
        
        // Parse CSV fields
        strcpy(players[numPlayers].name, token);
        token = strtok(NULL, ",");
        strcpy(players[numPlayers].club, token);
        token = strtok(NULL, ",");
        players[numPlayers].age = atoi(token);
        token = strtok(NULL, ",");
        players[numPlayers].potential = atoi(token);
        token = strtok(NULL, ",");
        strcpy(players[numPlayers].photoURL, token);

        // Check criteria
        if (players[numPlayers].age < 25 && players[numPlayers].potential > 85 && strcmp(players[numPlayers].club, "Manchester City") != 0) {
            // Print player information
            printf("Name: %s\n", players[numPlayers].name);
            printf("Club: %s\n", players[numPlayers].club);
            printf("Age: %d\n", players[numPlayers].age);
            printf("Potential: %d\n", players[numPlayers].potential);
            printf("Photo URL: %s\n", players[numPlayers].photoURL);
            printf("\n");
        }

        numPlayers++;
}
    fclose(file);

    return 0;
}
