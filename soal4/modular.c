#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <time.h>

#define PATH_MAX 256
#define CHUNK_SIZE 1024

void split_file(const char *file_path, int chunk_size);
void merge_files(const char *dir_path);
void modularize_directory(const char *path);
void log_event(const char *level, const char *cmd, const char *desc);

void split_file(const char *file_path, int chunk_size)
{
    FILE *file;
    char chunk_path[PATH_MAX * 2];
    char buffer[CHUNK_SIZE];
    size_t bytes_read;
    int chunk_count = 0;

    file = fopen(file_path, "rb");
    if (file == NULL)
    {
        printf("Cannot open file: %s\n", file_path);
        return;
    }

    while ((bytes_read = fread(buffer, 1, sizeof(buffer), file)) > 0)
    {
        snprintf(chunk_path, sizeof(chunk_path), "%s.%03d", file_path, chunk_count);
        FILE *chunk = fopen(chunk_path, "wb");
        if (chunk == NULL)
        {
            printf("Cannot create chunk file: %s\n", chunk_path);
            fclose(file);
            return;
        }
        fwrite(buffer, 1, bytes_read, chunk);
        fclose(chunk);
        chunk_count++;
    }

    fclose(file);
    remove(file_path);
    log_event("REPORT", "MODULARIZE", file_path);
}

void merge_files(const char *dir_path)
{
    DIR *dir;
    struct dirent *entry;
    char file_path[PATH_MAX];

    dir = opendir(dir_path);
    if (dir == NULL)
    {
        printf("Cannot open directory\n");
        return;
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG)
        {
            char *ext = strrchr(entry->d_name, '.');
            if (ext != NULL && strncmp(ext, ".000", 4) == 0)
            {
                snprintf(file_path, sizeof(file_path), "%s/%.*s", dir_path, (int)(ext - entry->d_name), entry->d_name);
                FILE *output_file = fopen(file_path, "wb");
                if (output_file == NULL)
                {
                    printf("Cannot create output file: %s\n", file_path);
                    closedir(dir);
                    return;
                }

                int chunk_number = 0;
                do
                {
                    snprintf(file_path, sizeof(file_path), "%s/%s.%03d", dir_path, entry->d_name, chunk_number);
                    FILE *chunk_file = fopen(file_path, "rb");
                    if (chunk_file == NULL)
                    {
                        printf("Cannot open chunk file: %s\n", file_path);
                        fclose(output_file);
                        closedir(dir);
                        return;
                    }

                    size_t bytes_read;
                    char buffer[CHUNK_SIZE];
                    while ((bytes_read = fread(buffer, 1, sizeof(buffer), chunk_file)) > 0)
                    {
                        fwrite(buffer, 1, bytes_read, output_file);
                    }

                    fclose(chunk_file);
                    chunk_number++;
                } while (chunk_number < 1000);

                fclose(output_file);
                log_event("REPORT", "MERGE", file_path);
            }
        }
    }

    closedir(dir);
}

void modularize_directory(const char *path)
{
    DIR *dir;
    struct dirent *entry;
    char sub_path[PATH_MAX];

    dir = opendir(path);
    if (dir == NULL)
    {
        printf("Cannot open directory\n");
        return;
    }

    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG)
        {
            char file_path[PATH_MAX];
            snprintf(file_path, sizeof(file_path), "%s/%s", path, entry->d_name);
            split_file(file_path, CHUNK_SIZE);
        }
        else if (entry->d_type == DT_DIR)
        {
            if (strncmp(entry->d_name, "module_", 7) == 0)
            {
                snprintf(sub_path, sizeof(sub_path), "%s/%s", path, entry->d_name);
                modularize_directory(sub_path);
            }
        }
    }

    closedir(dir);
}

void log_event(const char *level, const char *cmd, const char *desc)
{
    char log_path[PATH_MAX];
    time_t current_time = time(NULL);
    struct tm *time_info = localtime(&current_time);
    strftime(log_path, sizeof(log_path), "/home/nas/fs_module.log", time_info);

    FILE *log_file = fopen(log_path, "a");
    if (log_file == NULL)
    {
        printf("Cannot open log file: %s\n", log_path);
        return;
    }

    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", time_info);
    fprintf(log_file, "%s::%s::%s::%s\n", level, timestamp, cmd, desc);
    fclose(log_file);
}

int main()
{
    modularize_directory("home/nas/Documents/sisop/modul_4/soal4");
    merge_files("/home/nas/Documents/sisop/modul_4/soal4");

    return 0;
}

